# Curso de Econometría I #

Este curso es presentado en el V semestre de la carrera de economía de la [Universidad del Norte](https://www.uninorte.edu.co/) en la ciudad de Barranquilla, Colombia. Posee recursos académicos que serán complementos para los estudiantes del curso -tanto en la parte teórica como de la parte practica- donde se hace uso del software estadístico de [R](https://www.r-project.org/).

### Información General ###

* Asistir a todas las sesiones
* Algunas cosas serán compartidas en el **Brightspace**
* Los talleres y/o trabajos deben ser entregados en la carpeta para eso

### Presentaciones de la Clase ###

1. Generalidad [Introducción](https://bb.githack.com/Keynes37/econometria-i/raw/main/Class/Clase%2001%20intro/Clase1Econ.html)

2. Estructura de datos [EDA 1](https://bb.githack.com/Keynes37/econometria-i/raw/main/Class/Clase%2002%20EDA/Clase-EDA.html)

3. Inferencia básica [EDA 2](https://bb.githack.com/Keynes37/econometria-i/raw/main/Class/Clase%2003%20EDA/Clase-03-EDAII.html)

4. Modelo de Regresión [MCO](https://bb.githack.com/Keynes37/econometria-i/raw/main/Class/Clase%2004%20MCO/Clase04.html)

5. Inferencia en Regresión [Hipotesis](https://bb.githack.com/Keynes37/econometria-i/raw/main/Class/Clase%2005%20MCO%20INF/Clase05.html)

6. Regresión Múltiple [Variables Explicativas](https://bb.githack.com/Keynes37/econometria-i/raw/main/Class/Clase%2006%20REG%20MULT/Class06.html)

7. Regresión Múltiple [Variables Cualitativas](https://bb.githack.com/Keynes37/econometria-i/raw/main/Class/Clase%2007%20VarCual/Class07.html)

8. Heterocedásticidad [Supuestos MCO I](https://bb.githack.com/Keynes37/econometria-i/raw/main/Class/Clase%2008%20Heterocedasticidad/Class08.html)

9. Autocorrelación [Supuestos MCO II](https://bb.githack.com/Keynes37/econometria-i/raw/main/Class/Clase%2009%20Autocorrelacion/Class09.html)

10. Multicolinealidad [Supuestos MCO III](https://bb.githack.com/Keynes37/econometria-i/raw/main/Class/Clase%2010%20Causalidad/Class10.html)

11. No linealidad [Adicionales](https://bb.githack.com/Keynes37/econometria-i/raw/main/Class/Clase%2011%20No%20lineales/Class11.html)

